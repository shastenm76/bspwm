<h2>TUXER 76</h2>
<h1><em>SXHKD CONFIG BSPWM</em></h1>  

<p><u>El BSPWM es uno de mis entornos favoritos, y aquí están los atajos de mi configuración personal. Yo uso bsp-layout para configurar los dispocisiones diferentes de bsp, y luego uso dmenu para escogerlos. Puedes ver fotos de los disposiciones diferentes abajo. Tambien, uso polybar por la barra y picom por el opacidad. ¡Disfruta!</u></p> 
![bspwm](https://gitlab.com/shastenm76/bspwm/uploads/8aa44de0d0ee7853ef067f30ba1f168e/bspwm11.png)
<h5></em>Bspwm es utíl cuando necesitas tener dos cosas lado a lado.</em></h5>
![Normal Layout](https://gitlab.com/shastenm76/bspwm/uploads/ad7147127e6b6a4bd60f45a4a5f69734/bspwm3.png)
<h1><em>Mis Atajos</em></h1>
<h3><em>BIENVENIDOS<em></h3>
<h5><li>F12         Bienvenidos</li></h5>

<h3><em>ATAJOS MAS COMUNES</em></h3>
<ul>
    <li>Super + Return                Alacritty</li>
    <li>Super + d                     Dmenu</li>
    <li>Super + q                     Cerrar el node</li>
    <li>Super + 0-9                   Cambia el escritorio virtual</li>
    <li>Super + Shift + 0-9           Mover el node al escritorio virtual</li> 
    <li>Super + alt + Escape          Logout Menu</li>
</ul>
<h3><em>DMENU SCRIPTS</em></h3>
<ul>
    <li>Alt + a                Apps, docs, dot, keyboard,Man, PDF, Pipes, & Terminal</li> 
    <li>Alt + b                Cambia el color de Polybar</li>
    <li>Super + F10            Cambia el layout</li> 
    <li>Super + Alt + l        Cambia la idioma del teclado</li>
</ul>
<h3><em>PICOM COMPOSITE</em></h3>
<ul>
    <li>Alt + Shift + t        Opacidad</li>        
    <li>Alt + Shift + y        Composite</li>
</ul>
<h3><em>APPS</em></h3>
<ul>
    <li>Alt + c               Castero</li>         
    <li>Alt + i               Irssi</li>         
    <li>Alt + n               Newsboat</li>         
    <li>Ctrl + Shift + p      Pavucontrol</li>         
    <li>Alt + Shift + r       Rtv</li> 
    <li>Alt + r               Ranger</li>         
    <li>Alt + s               Spotify</li>         
    <li>Alt + w               Cambia los fondos del escritorio</li>         
    <li>Super + F11           Flameshot</li>         
    <li>Super + F12           Graba la pantalla completa con mplayer</li> 
</ul>

<h3><em>CAMBIA DE NODES</em></h3>
<ul>
        <li>Super (Shift) + n                        Mueve el enfoque al proximo node</li>         
        <li>Ctrl + Alt + (h,j,k,l)                   Cambia el node por otro</li>         
        <li>Super + (h,j,k,l)                        Cambia el enfoque del node</li> 
</ul>

<h3><em>CAMBIA LAYOUT, MOVER VENTANA FLOTANTE, Y GAPS</em></h3>
<ul>
        <li>Super + (f,w,e)                                     Pantalla completa, flotante, Mosaico</li>         
        <li>Super + Alt (Shift) + (Left,Down,up,Right)          Cambia el tamaño del node (máximo)</li>         
        <li>Ctrl + Alt + Shift + (Left,Down,up,Right)           Cambia el tamaño del node (mínimo)</li>         
        <li>Super + Alt + Equal                                 Gaps (aumento)</li>         
        <li>Super + Alt + Minus                                 Gaps (disminución)</li> 
        <li>Super + Alt + o                                     Gaps (quitar)</li>         
        <li>Super + Equal                                       Gaps (resetear)</li> 
</ul>
<h3><em>RATON</em></h3>
<ul>
        <li>Super + Click derecha                        cambia el tamaño del node</li>
</ul>


<h3></em>Fibonacci O Spiral Layout</em></h3>
<h5>Eso es el disposición por defecto en BSPWM.</h5>
![Fibonacci Layout](https://gitlab.com/shastenm76/bspwm/uploads/682a285fb685f349297e50704c7b7b1e/bspwm1.png)
<h3></em>Master And Stack Layout</em></h3>
<h5>Eso es el disposición por defecto en DWM.</h5>
![Master And Stack Layout](https://gitlab.com/shastenm76/bspwm/uploads/a41524aaa5e3909d9d7f58320d17fc25/bspwm2.png)
<h3></em>Reverse Master And Stack Layout</em></h3>
<h5>Eso es el Master And Stack al revez.</h5>
![Reverse Master And Stack Layout](https://gitlab.com/shastenm76/bspwm/uploads/d0248b2b195a9ff18560881db074466d/bspwm4.png)
<h3></em>Wide Layout</em></h3>
<h5>Eso es como Master And Stack pero es horizontal con el node principal arriba.</h5>
![Wide Layout](https://gitlab.com/shastenm76/bspwm/uploads/5411ae2154d0968fef839f65ce93549b/bspwm5.png)
<h3></em>Reverse Wide Layout</em></h3>
<h5>Eso es como Master And Stack pero es horizontal con el node principal abajo.</h5>
![Reverse Wide Layout](https://gitlab.com/shastenm76/bspwm/uploads/2aaa4668943f7aeee94bdca868ee6d6e/bspwm6.png)
<h3></em>Grid Layout</em></h3>
<h5>Eso hace los nodes iguales arriba y abajo.</h5>
![Grid Layout](https://gitlab.com/shastenm76/bspwm/uploads/f6afb40f2210e91726bca6d8fb32397d/bspwm7.png)
<h3></em>Reverse Grid Layout</em></h3>
<h5>Eso hace los nodes iguales arriba y abajo.</h5>
![Reverse Grid Layout](https://gitlab.com/shastenm76/bspwm/uploads/94f6a0629c69b3496a7d43d26e3d2e48/bspwm8.png)
<h3></em>Even LAYOUT</em></h3>
<h5>Eso hace que los nodes dividen de la misma área.</h5>
![Even Layout](https://gitlab.com/shastenm76/bspwm/uploads/a1a50e18347bdd3b4f8fc10340be61e6/bspwm10.png)
<h3></em>Monocle LAYOUT</em></h3>
<h5>Eso tiene un solo node que se ve y se esconde el resto.</h5>
![Monocle Layout](https://gitlab.com/shastenm76/bspwm/uploads/7347405189550c6e50fdfeec07bbe298/bspwm9.png)
